import {todoList} from './todoList';
import {ActionTypes} from "../actions/todoList";

describe('todo reducers', () => {

    it('should handle ADD_TODO', () => {
        expect(todoList(undefined, {
            type: ActionTypes.ADD_TODO,
            payload: {
                todo: {
                    id: 0,
                    name: 'помыть полы',
                    done: false,
                    edit: false
                }
            }
        })).toEqual({
                todoLists: [
                    {
                        id: 0,
                        name: 'помыть полы',
                        done: false,
                        edit: false
                    }
                ]
            })
    });

    it('should handle TOGGLE_TODO', () => {
       expect(todoList({todoLists: [{
               id: 0,
               name: 'помыть полы',
               done: false,
               edit: false
           }]}, {type: ActionTypes.TOGGLE_TODO, payload: { todoId: 0 }})).toEqual({
           todoLists: [
               {
                   id: 0,
                   name: 'помыть полы',
                   done: true,
                   edit: false
               }
           ]
       });
    });

    it('should handle DELETE_TODO', () => {
        expect(todoList({todoLists: [{
                id: 0,
                name: 'помыть полы',
                done: false,
                edit: false
            }]}, {type: ActionTypes.DELETE_TODO, payload: { todoId: 0 }})).toEqual({
            todoLists: []
        });
    });

    it('should handle CHANGE_EDIT_MODE_TODO', () => {
        expect(todoList({todoLists: [{
                id: 0,
                name: 'помыть полы',
                done: false,
                edit: false
            }]}, {type: ActionTypes.CHANGE_EDIT_MODE_TODO, payload: { todoId: 0 }})).toEqual({
            todoLists: [{
                id: 0,
                name: 'помыть полы',
                done: false,
                edit: true
            }]
        })
    });

    it('should handle SAVE_CHANGES_TODO', () => {
       expect(todoList({todoLists: [{
               id: 0,
               name: 'помыть полы',
               done: false,
               edit: true
           }]}, {type: ActionTypes.SAVE_CHANGES_TODO, payload: { todoId: 0, text: 'убраться' }})).toEqual({
            todoLists: [{
                id: 0,
                name: 'убраться',
                done: false,
                edit: false
            }]
        })
    });

    it('should handle GET_TODO_LIST', () => {
        expect(todoList({todoLists: []}, {type: ActionTypes.GET_TODO_LIST, payload: {
                todoList: [{
                    id: 0,
                    name: 'помыть полы',
                    done: false,
                    edit: false
                }]
            }})).toEqual({
            todoLists: [{
                id: 0,
                name: 'помыть полы',
                done: false,
                edit: false
            }]
        })
    });
});

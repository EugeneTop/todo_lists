import TodoList from "../models/todoList";
import {Action, ActionTypes} from "../actions/todoList";

export interface State {
    todoLists: TodoList[]
}

export const initialState: State = {
      todoLists: []
};

export function todoList(state: State = initialState, action: Action) {
    switch (action.type) {

        case ActionTypes.ADD_TODO: {
            const todoList = action.payload.todo;

            return {
                ...state,
                todoLists: [...state.todoLists, todoList]
            }
        }

        case ActionTypes.TOGGLE_TODO: {
            const todoId = action.payload.todoId;

            return {
                ...state,
                todoLists: state.todoLists.map(todo => todo.id === todoId ? {...todo, done: !todo.done} : todo)
            }
        }

        case ActionTypes.DELETE_TODO: {
            const todoId = action.payload.todoId;
            const todoLists = state.todoLists.filter(todo => todo.id !== todoId);
            todoLists.forEach((element, index) => {
                element.id = index;
            });

            return {
                ...state,
                todoLists: todoLists
            }
        }

        case ActionTypes.CHANGE_EDIT_MODE_TODO: {
            const todoId = action.payload.todoId;

            return {
                ...state,
                todoLists: state.todoLists.map(todo => todo.id === todoId ? {...todo, edit: !todo.edit} : todo)
            }
        }

        case ActionTypes.SAVE_CHANGES_TODO: {
            const todoId = action.payload.todoId;
            const todoText = action.payload.text;

            return {
                ...state,
                todoLists: state.todoLists.map(todo => todo.id === todoId ? {...todo, edit: !todo.edit, name: todoText} : todo)
            }
        }

        case ActionTypes.GET_TODO_LIST: {
            const todoList = action.payload.todoList;

            return {
                ...state,
                todoLists: todoList
            };

        }

        default: {
            return state;
        }
    }
}

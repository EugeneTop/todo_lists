export default interface TodoList {
    id: number,
    name: string,
    done: boolean,
    edit: boolean,
}

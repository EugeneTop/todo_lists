import {connect} from "react-redux";
import ViewTodoList from "../components/TodoList";
import {toggleTodo, deleteTodo, changeEditModeTodo, saveChangesTodo, getToDoList} from "../actions/todoList";

const mapStateToProps = (state: any) => ({
   todoList: state.todoList.todoLists
});

const mapDispatchToProps = {
    onTodoClicked: toggleTodo,
    onDeleteTodo: deleteTodo,
    onChangeEditMode: changeEditModeTodo,
    onSaveChanges: saveChangesTodo,
    setTodoList: getToDoList,
};

export default connect<any, any, any>(mapStateToProps, mapDispatchToProps)(ViewTodoList);

import {connect} from "react-redux";
import {addTodo} from "../actions/todoList"
import AddTodoForms from "../components/AddTodo";

const mapStateToProps = (state: any) => ({
    todoList: state.todoList.todoLists
});

const mapDispatchToProps = {
    handleSubmit: addTodo
};

export default connect<any, any, any> (mapStateToProps, mapDispatchToProps)(AddTodoForms);

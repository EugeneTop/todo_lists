import React from 'react';
import '../App.css';
import AddTodo from "../containers/AddTodo";
import ToDoList from "../containers/TodoList"

class App extends React.Component {

  public render() {

    return (
        <div className="App">
            <h1>To Do List</h1>
            <AddTodo/>
            <ToDoList/>
        </div>
    );
  }
}

export default App;

import React, {FormEvent} from "react";
import {TextField, Button} from "@material-ui/core";
import '../styles/AddTodo.css';
import TodoList from "../models/todoList";

interface Props {
    todoList: TodoList[];
    handleSubmit: (value: string, id: number) => void;
}

interface State {
    value: string;
}

export default class AddTodoForms extends React.Component<Props, State> {
    public constructor(props: Props) {
        super(props);
        this.state = { value: '' };
        this.updateValue = this.updateValue.bind(this);
        this.addTodoMessage = this.addTodoMessage.bind(this);
        this.clearField = this.clearField.bind(this);
    }

    private updateValue(value: string) {
        this.setState({ value });
    }

    private addTodoMessage(e: FormEvent<any>) {
        e.preventDefault();
        this.addTodo();
    }

    private addTodo() {
        if (!this.state.value.trim()) {
            return
        }

        const id = this.props.todoList.length;

        this.props.handleSubmit(this.state.value, id);
        this.setState({ value: '' });
    }

    private clearField() {
        if (!this.state.value.trim()) {
            return
        }

        this.setState({ value: '' })
    }

    private addMessageInTodoOnEnter(key: string): void {
        if(key === 'Enter'){
            this.addTodo();
        }
    }

    public render () {
        const { value } = this.state;
        const { updateValue, addTodoMessage, clearField } = this;

        return (
            <div>
                <div className="container_add_todo">
                    <div className="container_input_todo">
                        <TextField className="input_todo_list" value={value} onKeyPress={e => this.addMessageInTodoOnEnter(e.key)} onChange={e => updateValue(e.target.value)} />
                    </div>
                    <div className="container_forButton_todo">
                        <Button className="button_todo_list" onClick={addTodoMessage} type="submit" variant="contained" color="primary">
                            Добавить
                        </Button>
                    </div>
                    <div className="container_forButton_todo">
                        <Button className="button_todo_list" onClick={clearField} type="button" variant="contained" color="secondary">
                            Очистить
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

import TodoList from "../models/todoList";
import React from "react";
import '../styles/Todo.css';
import TodoNotEdit from "./TodoNotEdit";
import TodoWithEdit from "./TodoWithEdit";

interface Props {
    todoList: TodoList[];
    onTodoClicked: (todoId: number) => void;
    onDeleteTodo: (todoId: number) => void;
    onChangeEditMode: (todoId: number) => void;
    onSaveChanges: (todoId: number, text: string) => void;
    setTodoList: (todoList: TodoList[]) => void;
}

const TODO_LIST_NAME_IN_STORAGE = "todoList";

export default class ViewTodoList extends React.Component<Props> {

    public constructor(props: Props) {
        super(props);
        this.changeEditMode = this.changeEditMode.bind(this);
    }

    public componentDidMount() {
        const todoListJSON = localStorage.getItem(TODO_LIST_NAME_IN_STORAGE);

        if (todoListJSON !== null) {
            const todoList = JSON.parse(todoListJSON);
            this.props.setTodoList(todoList);
        }
    }

    public componentDidUpdate() {
        localStorage.setItem(TODO_LIST_NAME_IN_STORAGE, JSON.stringify(this.props.todoList));
    }

    private changeEditMode(id: number) {
        this.shutdownEditChange();
        this.props.onChangeEditMode(id);
    }

    private shutdownEditChange() {
        this.props.todoList.forEach((todo) => {
            if(!todo.edit) {
                return;
            }

            this.props.onChangeEditMode(todo.id);
        });
    }

    public render() {
        const {todoList, onTodoClicked, onDeleteTodo, onSaveChanges} = this.props;
        const { changeEditMode } = this;

        return (
            <div className="container_todo_lists">
                {
                    todoList.map((todo, index) => (
                        todo.edit ? <TodoWithEdit key={index} todo={todo} onSaveChanges={onSaveChanges}/> :
                            <TodoNotEdit key={index} todo={todo} onDeleteTodo={onDeleteTodo}
                                         onTodoClicked={onTodoClicked} onChangeEditMode={changeEditMode}/>
                    ))
                }
            </div>
        );
    }
}

import TodoList from "../models/todoList";
import React from "react";
import {Button, TextField} from "@material-ui/core";
import '../styles/Todo.css';

interface Props {
    todo: TodoList;
    onSaveChanges: (todoId: number, text: string) => void;
}

interface State {
    value: string;
}

export default class TodoNotEdit extends React.Component<Props, State>{

    public constructor(props: Props) {
        super(props);
        this.state = { value: props.todo.name };
        this.updateValue = this.updateValue.bind(this);
        this.updateMessageOnEnterInTodo = this.updateMessageOnEnterInTodo.bind(this);
    }

    private updateValue(value: string): void {
        this.setState({ value });
    }

    private updateMessageOnEnterInTodo(key: string): void {
        if(key === 'Enter'){
            const id = this.props.todo.id;
            const value = this.state.value;

            this.props.onSaveChanges(id, value);
        }
    }

    public render() {
        const { value } = this.state;
        const { todo, onSaveChanges } = this.props;

        return (
            <div className="container_row_todo">
                <div className="container_for_text_field_todo">
                    <TextField className="text_todo_edit" value={value} onKeyPress={e => this.updateMessageOnEnterInTodo(e.key)} onChange={e => this.updateValue(e.target.value)}/>
                </div>
                <div className="container_for_button_save">
                    <Button type="button" className="button_save button_todo" variant="contained" color="primary" onClick={() => onSaveChanges(todo.id, value)}>
                        Сохранить
                    </Button>
                </div>
            </div>
        );
    }
}
